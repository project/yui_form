
/**
 * Define a new Drupal behavior that attach our YUI Calendar widgets to
 * their JS handling instance.
 */

Drupal.behaviors.YUICalendarController = function(context) {
  $('.yui-form-calendar-widget', context).each(function(){
    if (! $(this).is('.yui-form-calendar-processed')) {
      new Drupal.YUICalendarController(context, this);
      $(this).addClass('yui-form-calendar-processed');
    }
  });
};

/**
 * Define an object that handle an instance of our widget. Each widget will
 * have its own instance of this object definition.
 */
Drupal.YUICalendarController = function(context, element) {
  /* Self pointing variable with larger scope than 'this' */
  var self = this;

  /* Some useful vars */
  this.uniqid = $(element).attr('id');
  this.context = context;
  this.dropDown = $(element).is('.drop-down');
  this.submitOnchange = $(element).is('.submit-onchange');
  this.dataInput = $('input.'+this.uniqid+'-data', this.context);
  this.calendar = null;

  if (! this.dropDown) {
    this.createCalendar();
  }
  else {
    $('.'+this.uniqid+'-show', this.context).click(function(){
      self.createCalendar();
      $(this).unbind('click').click(function(){
        self.refresh();
      });
    });
  }

  /* Handle external click */
  if (this.dropDown) {
    $(document.body).mousedown(function(event) {
      if (self.calendar != null) {
        var parents = $(event.target).parents();
  
        if ((parents.filter("#" + self.uniqid).length == 0)
            && (parents.filter("." + self.uniqid + "-show").length == 0))
        {
          self.calendar.hide();
        }
      }
    });
  }
};

Drupal.YUICalendarController.prototype.refresh = function(){
  /* Set default value */
  this.selectDateFromValue();
  this.calendar.render();

  //* Display our calendar in case of drop-down */
  if (this.dropDown) {
    this.calendar.show();
  }
};

Drupal.YUICalendarController.prototype.createCalendar = function(){
  var self = this;
  /* Create calendar */
  this.calendar = new YAHOO.widget.Calendar(self.uniqid, self.uniqid, {'close': this.dropDown});

  /* Dynamic properties */
  if (Drupal.settings.yui_calendar != undefined) {
    var settings = eval("Drupal.settings.yui_calendar." + this.uniqid);

    if (settings != undefined) {
      for(key in settings) {
    	this.calendar.cfg.setProperty(key, settings[key]);
      }
    }
  }

  /* Handle I18n, if present */
  if (Drupal.settings.yui_calendar_i18n != undefined) {
    this.calendar.cfg.setProperty("MONTHS_SHORT",    Drupal.settings.yui_calendar_i18n.month_short); 
    this.calendar.cfg.setProperty("MONTHS_LONG",     Drupal.settings.yui_calendar_i18n.month_long); 
    this.calendar.cfg.setProperty("WEEKDAYS_1CHAR",  Drupal.settings.yui_calendar_i18n.day_fchar); 
    this.calendar.cfg.setProperty("WEEKDAYS_SHORT",  Drupal.settings.yui_calendar_i18n.day_short); 
    this.calendar.cfg.setProperty("WEEKDAYS_MEDIUM", Drupal.settings.yui_calendar_i18n.day_medium); 
    this.calendar.cfg.setProperty("WEEKDAYS_LONG",   Drupal.settings.yui_calendar_i18n.day_long); 
  }

  /* Create onclick event */
  this.calendar.selectEvent.subscribe(function(type, args, obj) {
    // Get back selected date
    var dates = args[0];
    var date  = dates[0];
    var year  = new Number(date[0]);
    var month = new Number(date[1]);
    var day   = new Number(date[2]);
    var pickedUpDate = Drupal.YUICalendarController.mergeDate(year, month, day);

    self.dataInput.each(function(){
      this.value = pickedUpDate;
      $(this).triggerHandler('change');
    });

    /* Handle auto form submit on change */
    if (self.submitOnchange) {
      self.dataInput.each(function(){
        this.form.submit();
      });
    }
  }, this.calendar, true);

  /* Render */
  this.refresh();
};

Drupal.YUICalendarController.prototype.selectDateFromValue = function() {
  var calendar = this.calendar;

  this.dataInput.each(function(){
    var value = this.value;

    if (value != undefined && value.length == 10) {
      /* Set default value to textfield */
      var date = Drupal.YUICalendarController.sqlToArray(value);
      calendar.setMonth(parseInt(date[1], 10) - 1);
      calendar.setYear(parseInt(date[0], 10));

      /* Set property is for recent YUI */
      calendar.cfg.setProperty("selected", Drupal.YUICalendarController.toSelect(date));
    }
  });
};

/**
 * Convert a given number as a string, with preceding 0's until reached the
 * given len.
 * If the number string lengh is higher than len, does nothing.
 */
Drupal.YUICalendarController.flatNumber = function(n, len) {
  var str = n.toString();

  if ((append = (len - str.length)) > 0) {
    for (var i = 0; i < append; i++) {
      str = "0"+str;
    }
  }

  return str;
};

/**
 * From a given DATE sql formated string, return an array
 * Array(0 => year, 1 => month, 2 => day).
 */
Drupal.YUICalendarController.sqlToArray = function(date) {
  if (date != undefined) {
    return Array(date.substring(0,4), date.substring(5,7), date.substring(8,10));
  }
};

/**
 * From a given date array, return the DATE sql formated string.
 */
Drupal.YUICalendarController.mergeDate = function(year, month, day) {
  var date = Array();

  date.push(Drupal.YUICalendarController.flatNumber(year, 4));
  date.push(Drupal.YUICalendarController.flatNumber(month, 2));
  date.push(Drupal.YUICalendarController.flatNumber(day, 2));

  return date.join("-");
};

/**
 * From a given date array, return english formated date (MM/DD/YYYY).
 */
Drupal.YUICalendarController.toSelect = function(date) {
  return date[1]+'/'+date[2]+'/'+date[0];
};
