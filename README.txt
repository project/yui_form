
README.txt

Pre-requisites
==============
You must download and install yui module before using this one.
You can find it there: http://drupal.org/project/yui

Installation
============
Unpack the module in a Drupal's site module dir. Then enable it.

Usage
=====

See form elements samples which should live in your Drupal since you enabled
the module in admin/settings/yui/test

Notes
=====
This module should work with all YUI version >= 2.5. If not, please create an
appopriate issue on the project page to report it.

"I need X feature and Y tuning option"
======================================
Tow choices:
First one, try to do it yourself, then post a patch in the appropriate issue on
the project page.
Second one, ask us politely to do it in the appropriate issue on the project
page.
In both case, open the appropriate issue on the project page, and look up for
duplicates before doing it!
