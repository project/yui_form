<?php

function yui_form_admin_settings() {
  $form[YUI_FORM_USE_YUI_SKIN] = array(
    '#type' => 'checkbox',
    '#title' => t('Use yui module default skin'),
    '#default_value' => variable_get(YUI_FORM_USE_YUI_SKIN, TRUE),
  );
  return system_settings_form($form);
}

function yui_form_admin_ahah_test() {
  print drupal_to_js(array(
    'status' => TRUE,
    'data' => t('AHAH test succesful.'),
  ));
  exit();
}

function yui_form_test_form($form_state) {
  $form = array();

  $form['date-code'] = array(
    '#type' => 'markup',
    '#value' => '<h3>'.t('Date inline selection').'</h3><pre>'."  \$form['date'] = array(
    '#title' => t('Test calendar'),
    '#type' => 'yui_calendar',
    '#default_value' => '1983-03-22'
  );".'</pre>'
  );
  $form['date'] = array(
    '#title' => t('Test calendar'),
    '#type' => 'yui_calendar',
    '#default_value' => '1983-03-22'
  );

  $form['date-minmax-code'] = array(
    '#type' => 'markup',
    '#value' => '<h3>'.t('Minimum and maximum dates').'</h3><pre>'."  \$form['date-minmax'] = array(
    '#title' => t('Test calendar with max an min date'),
    '#type' => 'yui_calendar',
    '#default_value' => date('m/d/Y'),
    '#yui_options' => array(
      'mindate' => ''6/9/2009'',
      'maxdate' => date('m/d/Y'),
    ),
  );".'</pre>'
  );
  $form['date-minmax'] = array(
    '#title' => t('Test calendar with max an min date'),
    '#type' => 'yui_calendar',
    '#default_value' => date('m/d/Y'),
    '#yui_options' => array(
      'mindate' => '6/9/2009',
      'maxdate' => date('m/d/Y'),
    ),
  );

  $form['dropdowndate-code'] = array(
    '#type' => 'markup',
    '#value' => '<h3>'.t('Date drop-down selection').'</h3><pre>'."  \$form['dropdowndate'] = array(
    '#title' => t('Test calendar'),
    '#type' => 'yui_calendar',
    '#default_value' => '1983-03-22',
    '#drop_down' => TRUE,
  );".'</pre>'
  );
  $form['dropdowndate'] = array(
    '#title' => t('Test calendar'),
    '#type' => 'yui_calendar',
    '#default_value' => '1983-03-22',
    '#drop_down' => TRUE,
  );

  $form['dropdowndateahah-code'] = array(
    '#type' => 'markup',
    '#value' => '<h3>'.t('Date drop-down selection with AHAH').'</h3><pre>'."  \$form['dropdowndateahah'] = array(
    '#title' => t('Test AHAH with calendar'),
    '#description' => t('A messaged should appear on calendar change.'),
    '#type' => 'yui_calendar',
    '#default_value' => '1983-03-22',
    '#drop_down' => TRUE,
    '#suffix' => '&lt;div id=\"dropdowndateahah-target\" class=\"messages\"<>&gt;&lt;/div&gt;',
    '#ahah' => array(
      'event' => 'change',
      'path' => 'admin/settings/yui/ahah-test',
      'wrapper' => 'dropdowndateahah-target',
      'effect' => 'fade',
    ),
  );".'</pre>'
  );
  $form['dropdowndateahah'] = array(
    '#title' => t('Test AHAH with calendar'),
    '#description' => t('A messaged should appear on calendar change.'),
    '#type' => 'yui_calendar',
    '#default_value' => '1983-03-22',
    '#drop_down' => TRUE,
    '#suffix' => '<div id="dropdowndateahah-target" class="messages"></div>',
    '#ahah' => array(
      'event' => 'change',
      'path' => 'admin/settings/yui/ahah-test',
      'wrapper' => 'dropdowndateahah-target',
      'effect' => 'fade',
    ),
  );

  $form['percents-code'] = array(
    '#type' => 'markup',
    '#value' => '<h3>'.t('Simple horizontal slider').'</h3><pre>'."  \$form['percents'] = array(
    '#title' => t('Test slider'),
    '#type' => 'yui_slider',
    '#default_value' => 0.2,
    '#use_percent' => FALSE,
    '#live_display' => TRUE,
  );".'</pre>'
  );
  $form['percents'] = array(
    '#title' => t('Test slider'),
    '#type' => 'yui_slider',
    '#default_value' => 0.2,
    '#use_percent' => FALSE,
    '#live_display' => TRUE,
  );
  $form['submit'] = array(
    '#type' => 'submit',
    '#value' => t('Test it!'),
  );
  return $form;
}

function yui_form_test_form_submit($form, &$form_state) {
  drupal_set_message(t('Date is @date', array('@date' => $form_state['values']['date'])));
  drupal_set_message(t('Drop-down date is @date', array('@date' => $form_state['values']['dropdowndate'])));
  drupal_set_message(t('Slider is @percent', array('@percent' => $form_state['values']['percents'])));
}
