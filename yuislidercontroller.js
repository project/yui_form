
Drupal.behaviors.YUISliderController = function(context) {
  $('.yui-form-slider-widget', context).each(function(){
    if (! $(this).is('.yui-form-slider-processed')) {
      new Drupal.YUISliderController(context, this);
      $(this).addClass('yui-form-slider-processed');
    }
  });
};

Drupal.YUISliderController = function(context, element) {
  // Self pointing variable with larger scope than 'this'
  var self = this;

  // Some useful vars
  this.uniqid = $(element).attr('id');
  this.context = context;

  // Set up some widely used vars
  this.dataInput = $('input.'+this.uniqid+'-data', this.context);
  this.usePercent = $(element).is('.use_percent');
  this.liveDisplay = $(element).is('.live_display');
  if (this.liveDisplay) {
    this.liveDisplayContainer = $('#'+this.uniqid+'-live');
  }

  // Compute some slider variables
  var sliderId = this.uniqid,
      handleId = this.uniqid+"-handle",
  // Getting original control size
      bound = $('#'+handleId).width(),
      size = $('#'+sliderId).width() - bound;

  // Create slider
  var slider = YAHOO.widget.Slider.getHorizSlider(sliderId, handleId, 0, size + 2);
  slider.subscribe("change", function(offset) {
    if (! self.usePercent) {
      var value = offset / size;
    }
    else {
      var value = Math.round(offset / size * 100);
    }
    value = self.checkBounds(value);
    self.displayValue(value);
    self.dataInput.val(value);
  });

  // Apply value to slider
  var defaultValue = this.dataInput.val();
  if (defaultValue != undefined && defaultValue != null && defaultValue != '') {
    if (! this.usePercent) {
      defaultValue = defaultValue * 100;
    }
    defaultValue = Math.round(defaultValue * (size / 100));
    self.displayValue(defaultValue);
    slider.setValue(defaultValue, true);
  }
  else {
    this.displayValue(0);
  }
};

Drupal.YUISliderController.prototype.checkBounds = function(value) {
  // Transform our value in case we use indices
  if (! this.usePercent) {
    if (value < 0)
      value = 0;
    else if (value > 1)
      value = 1;
  }
  else {
    if (value < 0)
      value = 0;
    else if (value > 100)
      value = 100;
  }
  return value;
}

Drupal.YUISliderController.prototype.displayValue = function(value) {
  if (this.liveDisplay) {
    if (this.usePercent) {
      this.liveDisplayContainer.html(value+'%');
    }
    else {
      if (value != 0)
        this.liveDisplayContainer.html(Math.round(value * 100) / 100);
      else
        this.liveDisplayContainer.html('0');
    }
  }
};
