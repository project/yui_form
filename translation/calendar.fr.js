Drupal.behaviors.YUICalendarControllerI18n = function(context) {
  Drupal.settings.yui_calendar_i18n = {};
  Drupal.settings.yui_calendar_i18n.month_short = ["Jan.", "Fév.", "Mars", "Avr.", "Mai", "Juin", "Jui.", "Août", "Sep.", "Oct.", "Nov.", "Déc."];
  Drupal.settings.yui_calendar_i18n.month_long  = ["Janvier", "Février", "Mars", "Avril", "Mai", "Juin", "Juillet", "Août", "Septembre", "Octobre", "Novembre", "Décembre"];
  Drupal.settings.yui_calendar_i18n.day_fchar   = ["D", "L", "M", "M", "J", "V", "S"];
  Drupal.settings.yui_calendar_i18n.day_short   = ["Di", "Lu", "Ma", "Me", "Je", "Ve", "Sa"];
  Drupal.settings.yui_calendar_i18n.day_medium  = ["Dim", "Lun", "Mar", "Mer", "Jeu", "Ven", "Sam"];
  Drupal.settings.yui_calendar_i18n.day_long    = ["Dimanche", "Lundi", "Mardi", "Mercredi", "Jeudi", "Vendredi", "Samedi"];
}
