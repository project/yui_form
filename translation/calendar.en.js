Drupal.behaviors.YUICalendarControllerI18n = function(context) {
  Drupal.settings.yui_calendar_i18n = {};
  Drupal.settings.yui_calendar_i18n.month_short = ["Jan.", "Feb.", "Mar.", "Apr.", "May", "Jun.", "Jul.", "Aug.", "Sep.", "Oct.", "Nov.", "Dec."];
  Drupal.settings.yui_calendar_i18n.month_long  = ["January", "February", "March", "April", "May", "June", "July", "August", "September", "October", "November", "December"];
  Drupal.settings.yui_calendar_i18n.day_fchar   = ["S", "M", "T", "W", "T", "F", "S"];
  Drupal.settings.yui_calendar_i18n.day_short   = ["Su", "Mo", "Tu", "We", "Th", "Fr", "Sa"];
  Drupal.settings.yui_calendar_i18n.day_medium  = ["Sun", "Mon", "Tue", "Wed", "Thu", "Fri", "Sat"];
  Drupal.settings.yui_calendar_i18n.day_long    = ["Sunday", "Monday", "Tuesday", "Wednesday", "Thursday", "Friday", "Saturday"];
}
